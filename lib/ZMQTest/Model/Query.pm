package ZMQTest::Model::Query;
use Moo;
use ZMQ::FFI;
use ZMQ::FFI::Constants qw(ZMQ_PUSH ZMQ_PULL ZMQ_IO_THREADS);

has 'pull_endpoint' => (
	is => 'lazy',
);
sub _build_pull_endpoint {
	my $self = shift;
	my $s = $self->ctx->socket(ZMQ_PULL);
	$s->bind($self->endpoint);
	return $s;
}

has 'text' => (
	is => 'lazy',
);
sub _build_text {
	my $self = shift;
	my $s = $self->pull_endpoint();
	my $value = $s->recv();
	$s->close();
	return $value;
}

sub BUILD {
	my $self = shift;
	defined (my $pid = fork) or die "Cannot fork: $!";
	unless ($pid) {
	  my $s = $self->ctx->socket(ZMQ_PUSH);
	  $s->connect($self->endpoint);
	  my $content = "I'm queued data!";
	  $s->send($content);
	  exit;
	}
}

has 'ctx' =>(
	is => 'rw',
	required => 1,
);

has 'end' =>(
	is => 'ro',
	required => 1,
);

__PACKAGE__->meta->make_immutable;
1;