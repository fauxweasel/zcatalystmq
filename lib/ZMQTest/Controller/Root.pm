package ZMQTest::Controller::Root;
use Moose;
use namespace::autoclean;
use ZMQTest::Model::Query;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in ZMQTest.pm
#
__PACKAGE__->config(namespace => '');

=encoding utf-8

=head1 NAME

ZMQTest::Controller::Root - Root Controller for ZMQTest

=head1 DESCRIPTION

=head1 METHODS

=head2 index

The root page (/)

=cut

sub auto : Private() AllowAnonymous {
	my ( $self, $c ) = @_;
}

sub index :Path :Args(0) {
  my ( $self, $c ) = @_;

  my $query = ZMQTest::Model::Query->new();
  $c->stash( 
    template  => 'test.html',
  # execute   => sub { $query->queue() },
   text      => sub { $query->text() },

  );

  $c->detach($c->view("TT")); 
}

# sub index :Path :Args(0) {
#     my ( $self, $c ) = @_;

#     # Hello World
#     $c->response->body( $c->welcome_message );
# }

=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {}

=head1 AUTHOR

Ben Wasley,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
