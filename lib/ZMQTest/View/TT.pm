package ZMQTest::View::TT;
use Moose;
use namespace::autoclean;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.html',
    render_die => 1,
);

=head1 NAME

ZMQTest::View::TT - TT View for ZMQTest

=head1 DESCRIPTION

TT View for ZMQTest.

=head1 SEE ALSO

L<ZMQTest>

=head1 AUTHOR

Ben Wasley,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
